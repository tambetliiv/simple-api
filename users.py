from flask import Flask, request
from flask_restx import Api, Resource, fields
import pymongo
import os

# Define Flask app
app = Flask(__name__)
api = Api(app, version='1.0.0', title='User API',
          description='A Very Simple User API', prefix='/v1')

# Get mongo uri from env and connect to mongoDB
mongo_uri = os.environ.get('MONGO_URI', 'mongodb://db:27017')
mongo = pymongo.MongoClient(mongo_uri)

# Use simpleapi db
simpleapi = mongo.simpleapi

# Use users collection
users = simpleapi.users

# Define neccessary user fields to create user
user_fields = api.model('User', {
    'id': fields.Integer(required=True),
    'name': fields.String(required=True)
})


# /users endpoint
@api.route('/users')
class UserList(Resource):

    # List users if GET method
    def get(self, users=users):
        user_list = []
        for user in users.find({}):
            user_list.append(user['name'])
        return user_list, 200

    # With POST method validate input and add user to db
    @api.expect(user_fields, validate=True)
    def post(self, users=users):
        user_id = request.get_json()["id"]
        user_name = request.get_json()["name"]
        try:
            users.insert_one({"_id": int(user_id), "name": user_name})
        except pymongo.errors.DuplicateKeyError:
            return {"message": "User not added, id must be unique."}, 409
        return {"message": "User added."}, 200


# /users/{user_id} endpoint
@api.route('/users/<int:user_id>')
class UserId(Resource):
    # Get specific user by id
    def get(self, user_id, users=users):
        user = users.find_one({"_id": int(user_id)})
        if user is None:
            return {"message": "User with id: " +
                    str(user_id) + " not found."}, 404
        user["id"] = user.pop("_id")
        return user, 200


# Start API
if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
