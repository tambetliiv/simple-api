# Simple API

A Very Simple User API.

## Setting up dev environment

Prerequisites installed:
  - docker
  - docker-compose
```
# To bring dev env up, run
docker-compose up
```
Access API http://localhost:5000/

## Production deployment

Submit MR to https://gitlab.com/tambetliiv/simple-api and after merge, pipeline will deploy to https://simple-api.liiv.eu/

## Notes

API Auth was not configured as this was not in the scope of this task and this makes it easier to test this API.

If needed basic auth can be easily implemented, as API is running behind nginx ingress.
