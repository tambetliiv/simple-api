FROM python:3.11-alpine
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . /app
ENTRYPOINT ["gunicorn"]
CMD ["--conf", "gunicorn_conf.py", "--bind", "0.0.0.0:5000", "users:app"]
